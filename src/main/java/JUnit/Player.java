package JUnit;

public class Player {

     static int points;
     String name;
     String surname;

    public Player(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Player() {

    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


    public int addToPoints(int i, int b){
        points= i + b;
        return points;
    }

    public String testPoints(Player player) {
        player.points = points;
        if (points == 10){
            player.setPoints(5);
            player.addToPoints(3, 2);
            System.out.println(points);
            return String.valueOf(true);
        }
        return String.valueOf(false);
    }

}
