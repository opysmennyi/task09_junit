package JUnit;

import org.junit.jupiter.api.*;


;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Test_class {
    private final Player player = new Player("Anna", "Kokoszka");

    @BeforeAll
    public static void sout() {
        System.out.println("Lets test it");
    }
    @AfterAll
    public static void sout2() {
        System.out.println("Lets finish with test");
    }

    @Test
    void testMethod() {
        int i = 5;
        int b = 3;
        Player player = new Player();
        int res = player.addToPoints(i, b);
        assertEquals(8, res);
        System.out.println("Test well done");

    }
        @Test
        void testMethodFalse(){

        assertFalse(false, player.testPoints(player));
        }

        @Test
    void groupAssertions(){
            assertAll("player",
                    () -> assertEquals("Anna", player.getName()),
                    () -> assertEquals("Kokoszka", player.getSurname())
            );
            System.out.println(player.name + " " + player.surname);
        }

    @Test
    void dependentAssertions() {
        assertAll("properties",
                () -> {
                    String firstName = player.getName();
                    assertNotNull(firstName);
                    assertAll("name",
                            () -> assertTrue(firstName.startsWith("A")),
                            () -> assertTrue(firstName.endsWith("a"))
                    );
                },
                () -> {

                    String lastName = player.getSurname();
                    assertNotNull(lastName);
                    assertAll("last name",
                            () -> assertTrue(lastName.startsWith("K")),
                            () -> assertTrue(lastName.endsWith("a"))
                    );
                }
        );
        System.out.println("Test is compited " + player.name + " " + player.surname);
    }

}




